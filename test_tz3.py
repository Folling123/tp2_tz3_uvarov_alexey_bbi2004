import pytest
import math
import time
from datetime import timedelta
from tz3 import minimum, maximum, sum_of_numbers, composition_of_numbers

numbers = []
with open('bignumbers.txt','r') as biba:
    line = biba.readline()
    numbers = line.split()

numbers = list(map(int, numbers))


#Сравнение верности собственного нахождения минимума и python-версии
def test_minimum():
    assert minimum(numbers) == min(numbers)
#Сравнение верности собственного нахождения максимума и python-версии
def test_maximum():
    assert maximum(numbers) == max(numbers)
#Сравнение верности собственного нахождения суммы и python-версии
def test_sum_of_numbers():
    assert sum_of_numbers(numbers) == sum(numbers)
#Сравнение верности собственного нахождения произведения всех чисел и python-версии
def test_composition_of_numbers():
    assert composition_of_numbers(numbers) == math.prod(numbers)
#Проверка скорости работы всех функций
def test_of_time_of_minimum():
    start_time = time.time()
    minimum(numbers)
    lol = time.time() - start_time
    second_start_of_time = time.time()
    minimum(numbers[:5])
    lol1 = time.time() - second_start_of_time
    assert lol > lol1
    third_start_of_time = time.time()
    maximum(numbers)
    lol2 = time.time() - third_start_of_time
    fourth_start_of_time = time.time()
    maximum(numbers[:5])
    lol3 = time.time() - fourth_start_of_time
    assert lol2 > lol3
    fifth_start_of_time = time.time()
    sum_of_numbers(numbers)
    lol4 = time.time() - fifth_start_of_time
    sixth_start_of_time = time.time()
    sum_of_numbers(numbers[:5])
    lol5 = time.time() - sixth_start_of_time
    assert lol4 > lol5
    seventh_start_of_time = time.time()
    composition_of_numbers(numbers)
    lol6 = time.time() - seventh_start_of_time
    eighth_start_of_time = time.time()
    composition_of_numbers(numbers[:5])
    lol7 = time.time() - eighth_start_of_time
    assert lol6 > lol7

#Свой придуманный тест (Проверяем тип выводимых данных при придуманной функциии и python-версии

def test_of_type():
    assert type(maximum(numbers)) == type(max(numbers))









